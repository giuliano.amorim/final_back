const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const path = require('path');
const routes =  require('./src/routes');
const app = express();
const port = process.env.PORT || 3007;
const connectDB = require('./src/config/db');

app.use(cors());
app.use(cookieParser());
app.use(express.json());
app.use(routes);

// Connect Database
connectDB();


app.listen(port,function(){
    console.log(`Server runing on port ${port}`)
});